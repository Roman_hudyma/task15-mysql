SELECT maker,type FROM labor_sql.product
where type="Laptop"
order by maker;

SELECT model,ram,screen,price FROM labor_sql.laptop
WHERE price>1000
ORDER BY ram;

SELECT * FROM labor_sql.printer
WHERE color="y"
ORDER BY price DESC;

SELECT model,speed,hd,cd,price FROM labor_sql.pc
WHERE cd="12x" OR cd="24x" and price<600;

SELECT * FROM labor_sql.pc
WHERE speed>=500 and price<800
ORDER BY price DESC;

SELECT * FROM labor_sql.printer
WHERE type!="Matrix" and price>300
ORDER BY price DESC;

SELECT model,speed,hd FROM labor_sql.pc
WHERE hd>10 and hd<20 and 
EXISTS(SELECT maker from labor_sql.product 
WHERE pc.model=product.model)
ORDER BY speed;