SELECT maker,type,speed,hd 
FROM labor_sql.pc
JOIN labor_sql.product
on pc.model=product.model
WHERE hd>8;

SELECT maker 
FROM labor_sql.product
JOIN labor_sql.pc
ON product.model =pc.model
WHERE speed>=600;

SELECT maker 
FROM labor_sql.product
JOIN labor_sql.laptop
ON product.model =laptop.model
WHERE speed<=500;

SELECT pc.model,maker
FROM labor_sql.product
JOIN labor_sql.pc
ON product.model =pc.model
WHERE price<600;

SELECT printer.model,maker
FROM labor_sql.product
JOIN labor_sql.printer
ON product.model =printer.model
WHERE price<300;

SELECT maker,pc.model,price
FROM labor_sql.product
JOIN labor_sql.pc
ON product.model =pc.model;

SELECT maker,type,laptop.model,price
FROM labor_sql.product
JOIN labor_sql.laptop
ON product.model =laptop.model
WHERE speed>600;

SELECT name,country
FROM labor_sql.ships
JOIN labor_sql.classes
ON ships.class =classes.class;