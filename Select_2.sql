SELECT maker FROM labor_sql.product
WHERE EXISTS(SELECT model FROM labor_sql.pc 
WHERE product.model=pc.model);

SELECT maker FROM labor_sql.product
WHERE EXISTS(SELECT model FROM labor_sql.pc 
WHERE product.model=pc.model AND pc.speed>=750);

SELECT maker FROM labor_sql.product
WHERE EXISTS(SELECT model FROM labor_sql.pc 
WHERE exists(SELECT model FROM labor_sql.laptop
WHERE(product.model=pc.model AND pc.speed>=750) or (laptop.model=product.model and laptop.speed>=750)));

SELECT maker FROM labor_sql.product
WHERE EXISTS(SELECT max(price) FROM labor_sql.printer 
WHERE product.model=printer.model);

SELECT name,launched FROM labor_sql.ships
WHERE EXISTS(SELECT displacement FROM labor_sql.classes 
WHERE classes.displacement<35000 AND ships.class=classes.class AND ships.launched>1922);

SELECT maker FROM labor_sql.product
WHERE product.type!="Printer";

SELECT maker FROM labor_sql.product
WHERE type IN("Laptop","Printer");