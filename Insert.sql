USE mydb;

INSERT match_calendar(season,date)
VALUES("Cup of Italy",'2013-11-21'),
("Cup of Kuba",'2015-5-3'),
("Gracious match",'2012-12-30'),
("World championship",'2014-9-2'),
("Cup of Ukraine",'2015-10-13'),
("Demonstration match",'2015-2-18');

INSERT mydb.match(calendar_id,Status,Judge,Winner,Goal)
VALUES(1,"Abanndoned",null,null,null),
(2,"Played","Ivan petrenko","FC Barcelona",3),
(3,"Played","Sirio Forel","Chelsi",2),
(4,"Played","John Stricky","FK Karpaty",1),
(5,"Playef","Robbert Barateon",null,0),
(6,"Abanndoned",null,null,null);

INSERT judge(Name,Surname,Match_judged,Match_id)
VALUES("Ivan","Petrenko",22,2),
("Davyd","Ilnytskiy",12,2),
("Sirio","Forel",34,3),
("John","Stricky",14,4),
("Youriy","Borsuck",11,4),
("Rulan","Dubil",45,5);

USE mydb;
INSERT warning(Card,Player,Judge,Match_id)
VALUES();