-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Match_calendar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Match_calendar` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `season` VARCHAR(45) NOT NULL,
  `date` DATE NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Match`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Match` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `calendar_id` INT NOT NULL,
  `Status` VARCHAR(45) NULL,
  `Judge` VARCHAR(45) NULL,
  `Winner` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_match_calendar_idx` (`calendar_id` ASC) VISIBLE,
  CONSTRAINT `fk_match_calendar`
    FOREIGN KEY (`calendar_id`)
    REFERENCES `mydb`.`Match_calendar` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`judge`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`judge` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  `Surname` VARCHAR(45) NOT NULL,
  `Match_judged` INT NULL,
  `Match_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_judge_Match1_idx` (`Match_id` ASC) VISIBLE,
  CONSTRAINT `fk_judge_Match1`
    FOREIGN KEY (`Match_id`)
    REFERENCES `mydb`.`Match` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Goal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Goal` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Owner` VARCHAR(45) NULL,
  `Time_of_match` TIME NULL,
  `Match_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Goal_info_Match1_idx` (`Match_id` ASC) VISIBLE,
  CONSTRAINT `fk_Goal_info_Match1`
    FOREIGN KEY (`Match_id`)
    REFERENCES `mydb`.`Match` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Team`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Team` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  `Win` INT NULL,
  `Lose` INT NULL,
  `Total_game` INT NULL,
  `Total_player` INT NOT NULL,
  `Match_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Team_Match1_idx` (`Match_id` ASC) VISIBLE,
  CONSTRAINT `fk_Team_Match1`
    FOREIGN KEY (`Match_id`)
    REFERENCES `mydb`.`Match` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Player`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Player` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  `Surname` VARCHAR(45) NOT NULL,
  `goal` INT NULL,
  `Yellow_card` INT NULL,
  `Red_card` INT NULL,
  `Team_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Player_Team1_idx` (`Team_id` ASC) VISIBLE,
  CONSTRAINT `fk_Player_Team1`
    FOREIGN KEY (`Team_id`)
    REFERENCES `mydb`.`Team` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Warning`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Warning` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Card` VARCHAR(45) NOT NULL,
  `Player` VARCHAR(45) NOT NULL,
  `Judge` VARCHAR(45) NOT NULL,
  `Match_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Warning_Match1_idx` (`Match_id` ASC) VISIBLE,
  CONSTRAINT `fk_Warning_Match1`
    FOREIGN KEY (`Match_id`)
    REFERENCES `mydb`.`Match` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
