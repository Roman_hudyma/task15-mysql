SELECT maker FROM labor_sql.product
WHERE type IN("PC");

SELECT maker FROM labor_sql.product
WHERE type=ANY(SELECT type FROM labor_sql.product WHERE type ="PC");

SELECT maker FROM labor_sql.product
WHERE type =ALL(SELECT type from labor_sql.product WHERE type="PC");

SELECT maker FROM labor_sql.product
WHERE type IN("PC","Laptop") ;

SELECT maker FROM labor_sql.product
WHERE type =ANY(SELECT type FROM labor_sql.product WHERE type=("PC"OR"Laptop")) ;

SELECT maker,count(*) FROM labor_sql.product
WHERE maker ="A";

SELECT model,price FROM labor_sql.laptop
WHERE price>(SELECT MAX(price) from labor_sql.pc);
